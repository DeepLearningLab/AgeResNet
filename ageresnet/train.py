"""
This file is part of AgeResNet
Copyright (C) 2018 Çağrı Çatık, Jonas Mikolajczyk, Leonard Göhrs

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


import sys
import os

import torch
import torch.nn as nn
import torch.optim as optim

import numpy as np

from torch.utils.data import DataLoader

from ageresnet.dataset import SplitDataset
import ageresnet.helpers as helpers


class Trainer(object):
    def __init__(self, dataset, batch_size, network, device, mode):
        set_train, val_test_set = SplitDataset.split(dataset, 0.995, 4)
        set_val, set_test = SplitDataset.split(val_test_set, 0.80, 2)

        self.loader_train = DataLoader(set_train, batch_size=batch_size, shuffle=True, num_workers=1)
        self.loader_val = DataLoader(set_val, batch_size=batch_size)
        self.loader_test = DataLoader(set_test, batch_size=batch_size)

        self.mode = mode
        self.network = network

        self.network.set_mode(self.mode)

        self.optimizer = optim.Adam(
            self.network.select_parameters(mode),
            lr=0.01
        )

        self.scheduler = optim.lr_scheduler.ReduceLROnPlateau(
            self.optimizer, mode='min',
            factor=0.1, patience=6,
        )

        age_distribution = set_train.get_distribution()
        self.nll = nn.NLLLoss(age_distribution).to(device)

        self.device = device

    def loss_fn(self, output, labels):
        out_act_log = self.network.out_act_log(output)
        loss = self.nll(out_act_log, labels)

        return loss

    def score_fn(self, output, labels):
        _, idxes = output.detach().max(1)

        gender_scores = ((idxes / 100) == (labels / 100)).float() * 0.5
        age_scores = 0.5 - (abs((idxes % 100) - (labels % 100)).clamp(0, 10).float() / 20)

        scores = gender_scores + age_scores

        score = float(scores.mean())

        return score

    def softscore_fn(self, output, labels):
        out_act = self.network.out_act(output.detach().to('cpu'))

        idxes = np.arange(200).expand(out_act.size)
        lbls = labels.expand(out_act.size)

        gender_scores = ((idxes / 100) == (lbls / 100)).float() * 0.5
        age_scores = 0.5 - (abs((idxes % 100) - (lbls % 100)).clamp(0, 10).float() / 20)

        softscores = out_act * (gender_scores + age_scores)

        softscore = float(softscores.mean())

        return softscore

    def testset_sample(self):
        with torch.no_grad():
            images_cpu, _ = next(iter(self.loader_test))
            images_gpu = images_cpu.to(self.device)

            output_gpu = self.network.forward(images_gpu)
            output_cpu = output_gpu.to('cpu')

        images = images_cpu.detach()
        output = output_cpu.detach()

        output_sm = self.network.out_act(output)

        return (images_cpu, output_sm)

    def epoch(self):
        train_loss_acc = 0.0
        val_loss_acc = 0.0

        train_score_acc = 0.0
        val_score_acc = 0.0

        # Run through training set and update network
        self.network.set_mode(self.mode)

        for (train_batch_num, (images, labels)) in enumerate(self.loader_train):
            images = images.to(self.device)
            labels = labels.to(self.device)

            output = self.network.forward(images)
            loss = self.loss_fn(output, labels)
            score = self.score_fn(output, labels)

            del images
            del labels

            loss.backward()
            self.optimizer.step()
            self.optimizer.zero_grad()

            train_loss = float(loss.detach())
            train_loss_acc += train_loss
            train_score_acc += score

            del loss

        # Run through validation set
        self.network.set_mode('test')

        for (val_batch_num, (images, labels)) in enumerate(self.loader_val):
            images = images.to(self.device)
            labels = labels.to(self.device)

            output = self.network.forward(images)
            loss = self.loss_fn(output, labels)
            score = self.score_fn(output, labels)

            del images
            del labels

            val_loss = float(loss.detach())
            val_loss_acc += val_loss
            val_score_acc += score

            del loss

        train_loss_avg = train_loss_acc / (train_batch_num + 1)
        val_loss_avg = val_loss_acc / (val_batch_num + 1)

        train_score_avg = train_score_acc / (train_batch_num + 1)
        val_score_avg = val_score_acc / (val_batch_num + 1)

        # Update learning rate
        self.scheduler.step(val_loss_avg)
        learn_rate = self.optimizer.param_groups[0]['lr']

        return (train_loss_avg, val_loss_avg, train_score_avg, val_score_avg, learn_rate)


class LogHarness(object):
    def __init__(self, outdir, url, device):
        self.timer = helpers.RelTime()

        self.train_losses = list()
        self.val_losses = list()
        self.train_scores = list()
        self.val_scores = list()
        self.learn_rates = list()
        self.gpu_rams = list()
        self.gpu_max_rams = list()
        self.epoch_times = list()

        self.device = device
        self.url = url
        self.path_best_train = os.path.join(outdir, 'best_train_loss.pth')
        self.path_best_val = os.path.join(outdir, 'best_val_loss.pth')
        self.path_report = os.path.join(outdir, 'report.html')

    def log(self, prefix, text, flush=False):
        print('{} @ {:6} - {}'.format(prefix, int(self.timer.delta() + 0.5), text), flush=flush)

    def epoch(self, trainer):
        epoch_num = len(self.train_losses) + 1

        self.log('p', 'Run Epoch: {}'.format(epoch_num), True)
        train_loss, val_loss, train_score, val_score, learn_rate = trainer.epoch()

        gpu_max_ram = torch.cuda.max_memory_allocated(self.device.index) if self.device.type != 'cpu' else 0
        gpu_ram = torch.cuda.memory_allocated(self.device.index) if self.device.type != 'cpu' else 0

        self.log('t', 'Loss: {}'.format(train_loss))
        self.log('v', 'Loss: {}'.format(val_loss))

        self.train_losses.append(train_loss)
        self.val_losses.append(val_loss)
        self.train_scores.append(train_score)
        self.val_scores.append(val_score)
        self.learn_rates.append(learn_rate)
        self.gpu_max_rams.append(gpu_max_ram)
        self.gpu_rams.append(gpu_ram)
        self.epoch_times.append(self.timer.delta())

        if train_loss <= min(self.train_losses):
            trainer.network.save(self.path_best_train)

        if val_loss <= min(self.val_losses):
            trainer.network.save(self.path_best_val)

        test_images, test_labels = trainer.testset_sample()

        helpers.gen_report(
            self.path_report,
            self.train_losses, self.val_losses,
            self.train_scores, self.val_scores,
            self.gpu_rams, self.gpu_max_rams, self.learn_rates,
            test_images, test_labels,
            ' '.join(sys.argv),
            list(e - s for (e, s) in zip(self.epoch_times, [0] + self.epoch_times[:-1])),
        )

        if self.url:
            helpers.put_file(self.path_report, 'text/html', self.url)

        return (train_loss, val_loss, learn_rate)
