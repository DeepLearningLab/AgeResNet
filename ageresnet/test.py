"""
This file is part of AgeResNet
Copyright (C) 2018 Çağrı Çatık, Jonas Mikolajczyk, Leonard Göhrs

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


import numpy as np

from torchvision import transforms

from ageresnet.net import AgeResNet
import ageresnet.helpers as helpers

pil_process = transforms.Compose([
    transforms.Resize(160),
    transforms.CenterCrop(160),
    transforms.ToTensor(),
    transforms.Normalize(
        mean=[0.485, 0.456, 0.406],
        std=[0.229, 0.224, 0.225]
    ),
])

fw_process = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize(
        mean=[0.485, 0.456, 0.406],
        std=[0.229, 0.224, 0.225]
    ),
])

inv_process = transforms.Compose([
    transforms.Normalize(
        mean=[-0.485/0.229, -0.456/0.224, -0.406/0.225],
        std=[1/0.229, 1/0.224, 1/0.225]
    ),
    transforms.ToPILImage(),
])


class Tester(object):
    def __init__(self, net_path):
        self.device = helpers.find_accelerator()
        self.network = AgeResNet(None, net_path).to(self.device)

        self.network.set_mode('test')

    def predict(self, images):
        ages = list()
        genders = list()

        for image in images:
            img_tensor = fw_process((image / 256).astype(np.float32)).unsqueeze(0).to(self.device)

            age, gender = self.test(img_tensor)

            ages.append(age)
            genders.append({'F': 0, 'M': 1}[gender])

        ages = np.array(ages)
        genders = np.array(genders)

        return ages, genders

    def test_pil(self, image):
        img_tensor = pil_process(image.convert('RGB')).unsqueeze(0).to(self.device)

        return self.test(img_tensor)

    def test(self, img_tensor):
        dist = self.network.out_act(
            self.network.forward(img_tensor)
        ).to('cpu')

        prob, idx = dist.max(1)
        prob, idx = float(prob), int(idx)

        age = idx % 100
        gender = ('M', 'F')[idx // 100]

        return age, gender
