#!/usr/bin/env python3

"""
This file is part of AgeResNet
Copyright (C) 2018 Çağrı Çatık, Jonas Mikolajczyk, Leonard Göhrs

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


import argparse

import ageresnet.helpers as helpers

from ageresnet.net import AgeResNet
from ageresnet.dataset import ImdbWikiDataset, SplitDataset
from ageresnet.train import Trainer, LogHarness

def main():
    parser = argparse.ArgumentParser(description='Group 6 AgeResNet trainer')

    parser.add_argument('-b', '--minibatch', help='Minibatch size', required=False, default=256, type=int)
    parser.add_argument('-l', '--limit', help='Limit Dataset to n images', required=False, default=0, type=int)
    parser.add_argument('-i', '--imagenet', help='Imagenet base network', required=False, default='', type=str)
    parser.add_argument('-a', '--ageresnet', help='AgeResNet base network', required=False, default='', type=str)
    parser.add_argument('-d', '--dataset', help='Dataset Database', required=False, default='meta.db', type=str)
    parser.add_argument('-u', '--url', help='URL to PUT reports to', required=False, default=None, type=str)
    parser.add_argument('-o', '--outdir', help='Output directory', required=False, default='.', type=str)
    parser.add_argument('-p', '--preheat', help='Preheating Epoch limit', required=False, default=100, type=int)
    parser.add_argument('-e', '--epochs', help='Epoch limit', required=False, default=400, type=int)
    parser.add_argument('-g', '--git', help='Git Commit hash', required=False, default='', type=str)

    args = parser.parse_args()

    device = helpers.find_accelerator()
    network = AgeResNet(args.imagenet, args.ageresnet).to(device)
    dataset = ImdbWikiDataset(args.dataset)
    log_harness = LogHarness(args.outdir, args.url, device)

    if args.limit != 0:
        # Reduce dataset size for testing purposes
        dataset = SplitDataset(dataset, 0, args.limit)

    log_harness.log('s', 'Using device: {}, Batch size: {}'.format(device, args.minibatch))

    if args.imagenet:
        # Train only the fully connected output layer at first
        log_harness.log('s', 'Start preheating')
        trainer_preheat = Trainer(dataset, args.minibatch, network, device, 'preheat')

        for epoch_num in range(args.preheat):
            _, _, learn_rate = log_harness.epoch(trainer_preheat)

            if learn_rate <= 1e-5:
                break


    # Next train the whole network
    log_harness.log('s', 'Start baking')
    trainer_bake = Trainer(dataset, args.minibatch, network, device, 'bake')

    for epoch_num in range(args.epochs):
        _, _, learn_rate = log_harness.epoch(trainer_bake)

        if learn_rate <= 1e-6:
                break


if __name__ == '__main__':
    main()
