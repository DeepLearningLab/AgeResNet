#!/usr/bin/env python3

"""
This file is part of AgeResNet
Copyright (C) 2018 Çağrı Çatık, Jonas Mikolajczyk, Leonard Göhrs

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


import bottle as bo
import tempfile as tmp

class WebApi(bo.Bottle):
    def __init__(self, keys):
        super().__init__()

        self.uploads = dict()
        self.keys = keys

        self.get('/tiny_put/<key>', callback=self.get_key)
        self.put('/tiny_put/<key>', callback=self.put_key)

    def get_key(self, key):
        if key not in self.uploads:
            bo.abort(404, 'File not found')

        content_type, fd = self.uploads[key]

        bo.response.set_header('Content-Type', content_type)

        fd.seek(0)
        content = fd.read()

        return [content]

    def put_key(self, key):
        if key not in self.keys:
            bo.abort(404, 'File not found')

        content_type = bo.request.headers.get('Content-Type')

        fd = tmp.TemporaryFile()
        fd.write(bo.request.body.read())

        self.uploads[key] = (content_type, fd)

        return ['Ok!']

if __name__ == '__main__':
    import sys

    keys = sys.argv[1:]

    app = WebApi(keys)
    app.run(host='localhost', port=18080)
